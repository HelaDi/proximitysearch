import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class PositionalIndex {
	private HashMap<String, ArrayList<String>> invertedIndex; // Hashing term to DocID list.
	private HashMap<String, ArrayList<Integer>> positionIndex;// Hashing Term+DocID to position list.
	
	public PositionalIndex(){
		invertedIndex = new HashMap<String, ArrayList<String>>();
		positionIndex = new HashMap<String, ArrayList<Integer>>();
	}
	
	private boolean isDocIDExist(String term, String docID){
		if(invertedIndex.isEmpty()) 
			return false;
		
		for(int i = 0; i < invertedIndex.get(term).size(); i++){
			if(invertedIndex.get(term).get(i).equals(docID))
				return true;
		}
		return false;
	}
	
	private boolean isPositionExist(String term, String docID, int position){
		if(positionIndex.isEmpty())
			return false;
		for(int i = 0; i < positionIndex.get(term+docID).size(); i++){
			if(positionIndex.get(term+docID).get(i) == position)
				return true;
		}
		return false;
	}
	
	public void setPositionIntoDocID(String term, String docID, int position){
		ArrayList<String> tempPostingList = null;
		ArrayList<Integer> tempPositionList = null;
		
		if(invertedIndex.containsKey(term)){
			tempPostingList = invertedIndex.get(term);
			if(!isDocIDExist(term, docID)){
				tempPostingList.add(docID);
				if(positionIndex.containsKey(term+docID)){
					tempPositionList = positionIndex.get(term+docID);
					if(!isPositionExist(term, docID, position)){
						tempPositionList.add(position);
					}
				}else{
					tempPositionList = new ArrayList<Integer>();
					tempPositionList.add(position);
					positionIndex.put(term+docID, tempPositionList);
				}
			}
		}else{
			tempPostingList = new ArrayList<String>();
			tempPostingList.add(docID);
			invertedIndex.put(term, tempPostingList);
			tempPositionList = new ArrayList<Integer>();
			tempPositionList.add(position);
			positionIndex.put(term+docID, tempPositionList);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void sortInvertedIndex(){
		@SuppressWarnings("rawtypes")
	Iterator itr = invertedIndex.entrySet().iterator();

		while(itr.hasNext()){
			@SuppressWarnings("rawtypes")
		HashMap.Entry<String, ArrayList<String>> entry = (HashMap.Entry) itr.next();
			Collections.sort(entry.getValue());
		}
		
		itr = positionIndex.entrySet().iterator();
		while(itr.hasNext()){
			@SuppressWarnings("rawtypes")
			HashMap.Entry<String, ArrayList<String>> entry = (HashMap.Entry) itr.next();
			Collections.sort(entry.getValue());
		}
	}
	
	public ArrayList<Answer> proximityIntersec(int k, String term1, String term2){
		ArrayList<String> term1PostingList = invertedIndex.get(term1);
		ArrayList<String> term2PostingList = invertedIndex.get(term2);
		
		int i = 0;
		int j = 0;
		int compareResult;
		ArrayList<Answer> answers = new ArrayList<Answer>();
		Answer tempAnswer = null;
		ArrayList<Integer> positionResult = new ArrayList<Integer>();
		ArrayList<Integer> term1PositionList = null;
		ArrayList<Integer> term2PositionList = null;
		while(i != term1PostingList.size() && j != term2PostingList.size()){
			compareResult = term1PostingList.get(i).compareTo(term2PostingList.get(j));
			if(compareResult == 0){
				positionResult.clear();
				term1PositionList = positionIndex.get(term1+term1PostingList.get(i));
				term2PositionList = positionIndex.get(term2+term2PostingList.get(j));
				int m = 0;
				int n = 0;
				while(m != term1PositionList.size()){
					while(n != term2PositionList.size()){
						if(Math.abs(term1PositionList.get(m) - term2PositionList.get(n)) <= k)
							positionResult.add(term2PositionList.get(n));
						else
							break;
						n++;
					}
					while(!positionResult.isEmpty() 
							&& Math.abs(positionResult.get(0) - term1PositionList.get(m)) > k){
						positionResult.remove(0);
					}
					if(!positionResult.isEmpty()){
						tempAnswer = new Answer();
						tempAnswer.docID = term1PostingList.get(i);
						tempAnswer.pp1 = term1PositionList.get(m); 
						for(int index = 0; index < positionResult.size(); index++){
							tempAnswer.pp2List.add(positionResult.get(index));
						}
						answers.add(tempAnswer);
					}
					m++;
				}
				i++;
				j++;
			}else if(compareResult < 0)
				i++;
			else
				j++;
		}
		return answers;
	}
	
	public ArrayList<Answer> inOrderIntersec(int k, String term1, String term2){
		ArrayList<String> term1PostingList = invertedIndex.get(term1);
		ArrayList<String> term2PostingList = invertedIndex.get(term2);
		
		int i = 0;
		int j = 0;
		int compareResult;
		ArrayList<Answer> answers = new ArrayList<Answer>();
		Answer tempAnswer = null;
		ArrayList<Integer> positionResult = new ArrayList<Integer>();
		ArrayList<Integer> term1PositionList = null;
		ArrayList<Integer> term2PositionList = null;
		while(i != term1PostingList.size() && j != term2PostingList.size()){
			compareResult = term1PostingList.get(i).compareTo(term2PostingList.get(j));
			if(compareResult == 0){
				positionResult.clear();
				term1PositionList = positionIndex.get(term1+term1PostingList.get(i));
				term2PositionList = positionIndex.get(term2+term2PostingList.get(j));
				int m = 0;
				int n = 0;
				while(m != term1PositionList.size()){
					while(n != term2PositionList.size()){
						if(Math.abs(term1PositionList.get(m) - term2PositionList.get(n)) == k
								&& term1PositionList.get(m) < term2PositionList.get(n))
							positionResult.add(term2PositionList.get(n));
						else
							break;
						n++;
					}
					while(!positionResult.isEmpty() 
							&& (Math.abs(positionResult.get(0) - term1PositionList.get(m)) > k
									|| Math.abs(positionResult.get(0) - term1PositionList.get(m)) < k)){
						positionResult.remove(0);
					}
					
					if(!positionResult.isEmpty()){
						tempAnswer = new Answer();
						tempAnswer.docID = term1PostingList.get(i);
						tempAnswer.pp1 = term1PositionList.get(m); 
						for(int index = 0; index < positionResult.size(); index++){
							tempAnswer.pp2List.add(positionResult.get(index));
						}
						answers.add(tempAnswer);
					}
					m++;
				}
				i++;
				j++;
			}else if(compareResult < 0)
				i++;
			else
				j++;
		}
		return answers;
	}
}

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ProximitySearch {
	
	public static void main(String[] args) throws IOException{
		String fileName = null;
		if(args.length == 0)
			return;
		
		fileName = args[0];
		PositionalIndex positionalIndex = Parser.parsingFile(fileName);
		System.out.println("Please enter your query.");
		Scanner scanner = new Scanner(System.in);
		
		String query;  
		String[] sTokens;
		String tempTerm1;
		String tempTerm2;
		String operator;
		int tempK;
		ArrayList<Answer> answerList = new ArrayList<Answer>();
		
		while(!(query = scanner.nextLine()).equals(":q")){
			sTokens = query.split(" ");
			tempTerm1 = sTokens[0];
			operator = Character.toString(sTokens[1].charAt(0));

			if(!operator.equals("/") && !operator.equals("=")){
				System.out.println("Sorry, please use right operator, \"/\" or \"=\".");
				continue;
			}
			tempK = Integer.parseInt(sTokens[1].substring(1));
			tempTerm2 = sTokens[2];
			if(operator.equals("/"))
				answerList = positionalIndex.proximityIntersec(tempK, tempTerm1, tempTerm2);
			
			if(operator.equals("="))
				answerList = positionalIndex.inOrderIntersec(tempK, tempTerm1, tempTerm2);
			
			if(!answerList.isEmpty()){
				for( int i = 0; i < answerList.size(); i++){
					System.out.println("The term " + tempTerm1 + " and term " + tempTerm2 + " in the same document " + answerList.get(i).docID + ".");
					System.out.print("The positions are " + answerList.get(i).pp1 + " of " + tempTerm1 + " with corresponding " + tempTerm2 + " whose positions are ");
					for(int j = 0; j < answerList.get(i).pp2List.size(); j++){
						if (j == answerList.get(i).pp2List.size() - 1)
							System.out.println(answerList.get(i).pp2List.get(j) + ". ");
						else
							System.out.println(answerList.get(i).pp2List.get(j) + ", ");
					}
				}
			}else
				System.out.println("Sorry, no result match.");
			System.out.println("Please enter your query.");
		}
	}
}

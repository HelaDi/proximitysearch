import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Parser {
	
	public static PositionalIndex parsingFile(String filename) throws IOException{
		
		FileReader file = new FileReader(filename);
		BufferedReader text = new BufferedReader(file);
		String line;
		String[] tokens;
		PositionalIndex positionalIndex = new PositionalIndex();
		
		while((line = text.readLine()) != null){
			tokens = line.split("\\W+");			
			 for( int i = 1; i < tokens.length; i++ ){
				 positionalIndex.setPositionIntoDocID(tokens[i], tokens[0], i-1);
			 }
			Arrays.fill(tokens, null); 
		}
		text.close();
		positionalIndex.sortInvertedIndex();
	  return positionalIndex;
	}
}

